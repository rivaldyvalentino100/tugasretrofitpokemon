package com.tugas1.tugasretrofitpokemon.data.model

import com.google.gson.annotations.SerializedName

data class BaseResponPokemon (
    @SerializedName("next")
    val next: String,
    @SerializedName("previous")
    val previous: Any,
    @SerializedName("results")
    val pokemonResults: List<ResultPokemon>
)