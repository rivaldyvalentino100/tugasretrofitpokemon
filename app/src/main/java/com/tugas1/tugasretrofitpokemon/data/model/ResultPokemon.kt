package com.tugas1.tugasretrofitpokemon.data.model

import com.google.gson.annotations.SerializedName

class ResultPokemon (
    @SerializedName("name")
    val name: String,
    @SerializedName("url")
    val url: String
)