package com.tugas1.tugasretrofitpokemon.data.network

import com.tugas1.tugasretrofitpokemon.data.model.BaseResponPokemon
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("pokemon")
    fun getPokemons() : Call<BaseResponPokemon>
}