package com.tugas1.tugasretrofitpokemon.data.repository

import com.tugas1.tugasretrofitpokemon.data.model.BaseResponPokemon
import com.tugas1.tugasretrofitpokemon.data.model.ResultPokemon
import com.tugas1.tugasretrofitpokemon.data.network.NetworkClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepositoryApi {

    fun fetchPokemon(onSuccess:(data:List<ResultPokemon>)->Unit, onFailed:(message:String)->Unit){
        NetworkClient.instance.getPokemons().enqueue(object : Callback<BaseResponPokemon> {
            override fun onResponse(
                call: Call<BaseResponPokemon>,
                response: Response<BaseResponPokemon>
            ) {
                if (response.code() == 200){
                    val data = response.body()?.pokemonResults?: mutableListOf()
                    onSuccess(data)

                }
            }

            override fun onFailure(call: Call<BaseResponPokemon>, t: Throwable) {
                onFailed(t.message?:"")
            }

        })


    }
}