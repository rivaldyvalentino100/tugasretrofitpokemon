package com.tugas1.tugasretrofitpokemon.view.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tugas1.tugasretrofitpokemon.data.model.ResultPokemon
import com.tugas1.tugasretrofitpokemon.data.repository.RepositoryApi

class MainViewModel : ViewModel() {


    val resulSuccesstPokemon = MutableLiveData<List<ResultPokemon>>()
    val resulFailedtPokemon = MutableLiveData<String>()

    val repositoryApi = RepositoryApi()

    fun fetchPokemon(){
        repositoryApi.fetchPokemon(onSuccess = {
            resulSuccesstPokemon.postValue(it)
        },onFailed = {
            resulFailedtPokemon.postValue(it)
        })


    }
}