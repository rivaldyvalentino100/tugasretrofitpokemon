package com.tugas1.tugasretrofitpokemon.view.viewModel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tugas1.tugasretrofitpokemon.R
import com.tugas1.tugasretrofitpokemon.data.model.ResultPokemon
import com.tugas1.tugasretrofitpokemon.databinding.ItemPokemonBinding

class PokemonAdapter(val pokemons:List<ResultPokemon>) : RecyclerView.Adapter<PokemonViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_pokemon,
            parent, false)
        return PokemonViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        holder.bind(pokemons[position])
    }

    override fun getItemCount(): Int {
        return pokemons.size
    }
}


class PokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

    var binding: ItemPokemonBinding = ItemPokemonBinding.bind(itemView)

    fun bind(itemPokemon: ResultPokemon){
        Glide.with(itemView)
            .load(getImageUrl(itemPokemon.url))
            .into(binding.ivPokemon)
        binding.tvPokemonName.text = itemPokemon.name
    }

    private fun getImageUrl(url:String): String {
        val index = url.split("/".toRegex()).dropLast(1).last()
        return "https://pokeres.bastionbot.org/images/pokemon/$index.png"
    }
}