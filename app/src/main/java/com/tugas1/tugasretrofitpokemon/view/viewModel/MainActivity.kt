package com.tugas1.tugasretrofitpokemon.view.viewModel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.tugas1.tugasretrofitpokemon.R
import com.tugas1.tugasretrofitpokemon.data.model.ResultPokemon
import com.tugas1.tugasretrofitpokemon.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var adapterPokemon: PokemonAdapter
    lateinit var pokemons:MutableList<ResultPokemon>
    private val viewModel:MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        pokemons = mutableListOf()
        adapterPokemon = PokemonAdapter(pokemons)
        binding.rvPokemon.apply {
            layoutManager = GridLayoutManager(this@MainActivity,2)
            adapter = adapterPokemon
        }

        viewModel.resulSuccesstPokemon.observe(this,{
            manageStateResponseSuccess(it)
        })

        viewModel.resulFailedtPokemon.observe(this,{
            manageStateResponseFailed(it)
        })

        viewModel.fetchPokemon()


    }

    private fun manageStateResponseSuccess(response: List<ResultPokemon>?){


        if (response !=null){
            binding.pbProgress.visibility = View.GONE
            binding.rvPokemon.visibility = View.VISIBLE
            if (response.isNotEmpty()) {
                pokemons.addAll(response)
                adapterPokemon.notifyDataSetChanged()
            }
        }else{
            binding.pbProgress.visibility = View.VISIBLE
        }



    }

    private fun manageStateResponseFailed(response: String?){


        if (response !=null){
            binding.pbProgress.visibility = View.GONE
            Toast.makeText(this,response, Toast.LENGTH_LONG).show()
        }



    }
    }
